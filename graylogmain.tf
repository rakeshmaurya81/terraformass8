provider "aws" {
  region = "us-east-1"
access_key = "AKIARUNBT2N45LDJWSMVEXTRA"
    secret_key = "csbRk5jZ7isVn2hLbhdBiZpNXp1RH4bscqbBvpIx"
}

resource "aws_vpc" "myvpc" {
  cidr_block           = var.cidr_block
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "ninja-vpc-01"
  }
}

#Create Internet Gateway in VPC
resource "aws_internet_gateway" "myigw" {
  vpc_id = aws_vpc.myvpc.id
  tags = {
    Name = var.igw_name
  }
}

#Create Public Subnet in multi Availability Zone
resource "aws_subnet" "public-Subnet" {
  count                   = length(var.pubic-subnet-cidr)
  vpc_id                  = aws_vpc.myvpc.id
  cidr_block              = element(var.pubic-subnet-cidr, count.index)
  availability_zone       = element(var.azs, count.index)
  map_public_ip_on_launch = true
  tags = {
    Name = var.pub_sub_name[count.index]
  }
}


#Create Route Table and Add Public Route
#terraform aws create route table
resource "aws_route_table" "pub-route-table" {
  vpc_id = aws_vpc.myvpc.id

  route {
    cidr_block = var.routetb_cidr
    gateway_id = aws_internet_gateway.myigw.id
  }
  tags = {
    Name = var.pub_routetb
  }
}

#Associate route table to Public subnet
resource "aws_route_table_association" "public" {
  count          = length(var.pub_sub_name)
  subnet_id      = aws_subnet.public-Subnet[count.index].id
  route_table_id = aws_route_table.pub-route-table.id
}

#Create Private Subnet in multi Availability Zone
resource "aws_subnet" "private-Subnet" {
  count                   = length(var.private-subnet-cidr)
  vpc_id                  = aws_vpc.myvpc.id
  cidr_block              = element(var.private-subnet-cidr, count.index)
  availability_zone       = element(var.azs, count.index)
  map_public_ip_on_launch = false
  tags = {
    Name = var.priv_sub_name[count.index]
  }
}

#Generate Elastic IP
resource "aws_eip" "NatEip" {
  vpc = true
}

#Create Nat Gateway in Public Subnet
resource "aws_nat_gateway" "nat" {
  #count          = length(var.pub_sub_name)
  subnet_id     = aws_subnet.public-Subnet[0].id
  allocation_id = aws_eip.NatEip.id
  tags = {
    Name = var.nat_name
  }

}

#Create Route Table and Add Private Route
#terraform aws create route table
resource "aws_route_table" "priv-route-table" {
  vpc_id = aws_vpc.myvpc.id

  route {
    cidr_block = var.routetb_cidr
    gateway_id = aws_nat_gateway.nat.id
  }
  tags = {
    Name = var.priv_routetb
  }

}

resource "aws_route_table_association" "private" {
  count          = length(var.priv_sub_name)
  subnet_id      = aws_subnet.private-Subnet[count.index].id
  route_table_id = aws_route_table.priv-route-table.id
}




