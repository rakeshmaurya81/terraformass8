variable "cidr_block" {
  type        = string
  default     = "10.0.0.0/16"
  description = "CIDR block for the VPC"
}

variable "igw_name" {
  default = "igw-01"
}

variable "pubic-subnet-cidr" {
  default     = ["10.0.0.0/24", "10.0.1.0/24"]
  description = "public-subnet-1-cidr"
}
variable "pub_sub_name" {
  default = ["public-subnet-1", "public-subnet-2"]
}

variable "azs" {
  type    = list(any)
  default = ["us-east-1a", "us-east-1b"]
}

variable "routetb_cidr" {
  default = "0.0.0.0/0"
}

variable "pub_routetb" {
  default = "pub-RT"
}

variable "private-subnet-cidr" {
  default     = ["10.0.2.0/24", "10.0.3.0/24"]
  description = "private-subnet-1-cidr"
}
variable "priv_sub_name" {
  default = ["private-subnet-1", "private-subnet-2"]
}

variable "nat_name" {
  default = "Nat-Gatwey-01"
}

variable "priv_routetb" {
  default = "priv-RT"
}

variable "priv_instance" {
  default = ["DB-Server", "NginxServer"]
}




