resource "aws_security_group" "pub-sg" {
  name        = "pub-sg"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.myvpc.id
  ingress {
    description = "TLS from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["103.89.59.158/32"] #allow vpc network
  }

  ingress {
    description = "TLS from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["103.89.59.158/32"]
  }

  ingress {
    description = "TLS from VPC"
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "pub-sg"
  }
}

resource "aws_security_group" "priv-sg" {
  name        = "priv-sg"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.myvpc.id
  ingress {
    description = "TLS from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["103.89.59.158/32"] #allow vpc network
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["103.89.59.158/32"]
  }

  ingress {
    description = "TLS from VPC"
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "priv-sg"
  }
}

#resource "tls_private_key" "new_key1" {
#  algorithm   = "RSA"
#  rsa_bits = "2048"
#}

resource "aws_key_pair" "new_key" {
  key_name   = "terra123"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDggzsX2u2fgrrA6EbOAL8NWDOqONqtdf+562z+sPrr8K7vbvUamngeBgPw0pqZ4DoO5hIU7NPgL/FAsG/6YWZg3fQOuOlCcDyBnNWre4SNVMiKMF6isZc19A11I49okc9QsKQYUBePSOqI2MJV7pPaQ+QoDzc/oc2iYCybmqbXOchjQyTss/MG/5fjRqy4i/7YaZPHwGS7v2P+E8i6pTIlmhvIj7IVEQyJXXrh8JcMKL6AHDlK00hLlzp1Oi4Cvk+zwm9Vw1zMM8ORxVVMxagZObomdfMsHU0Eqmv8M+DT5dxc6FvrONVpoHBzlFLVg6k1+HDV3zWgpItShdWzt6MagkXjaB3EyMKuvbwPZRnsisp5waPrKf7urQ0hcDrcSz+R+3QR2LUzbr/XZNvqcUldXo+pWL34GUbqhcvN6oBZP4TTCOwRDg8+kTQ7Bc6zYAnu57f9QWlAh+uDv3qLwP5p5t/tmNcI05cWQ4QYmlOuL4Itjn2i/zVnidXj4yy1u50= reyanshshreya@rakesh"
}


#Creating EC2 in Publice Subnet
resource "aws_instance" "public_instance" {
  ami                          = "ami-083654bd07b5da81d"
  instance_type                = "t2.micro"
  count                        = length(var.pub_sub_name)
  subnet_id                    = aws_subnet.public-Subnet[count.index].id
  security_groups              = [aws_security_group.pub-sg.id]
  key_name                     = "${aws_key_pair.new_key.key_name}"
  associate_public_ip_address = true
  tags = {
    Name = "varpending"
  }
}

#Creating EC2 in Private Subnet
resource "aws_instance" "private_instance" {
  ami                          = "ami-083654bd07b5da81d"
  instance_type                = "t2.micro"
  count                        = length(var.priv_sub_name)
  subnet_id                    = aws_subnet.private-Subnet[count.index].id
  security_groups              = [aws_security_group.priv-sg.id]
  key_name                     = "${aws_key_pair.new_key.key_name}"
  associate_public_ip_address = false
  tags = {
    #count = length(var.priv_instance)
   # Name  = [count.index]
  }
}